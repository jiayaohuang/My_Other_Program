#
# /etc/bash.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[[ $DISPLAY ]] && shopt -s checkwinsize

HISTCONTROL=ignoreboth

## use \[ \] in both side of invisible character to fix Line Break issue.
## http://mywiki.wooledge.org/BashFAQ/053
if [ "$USER" == "root" ]; then
	PS1='\[\e[1;31m\][\u@\h \w]\[\e[0m\] \[\e[0;36m\]<\D{%F} \t>\[\e[0m\]
\[\e[1;31m\]\$\[\e[0m\] '
else
	PS1='\[\e[1;32m\][\u@\h \w]\[\e[0m\] \[\e[0;36m\]<\D{%F} \t>\[\e[0m\]
\[\e[1;32m\]\$\[\e[0m\] '
fi

PS2='> '
PS3='> '
PS4='+ '

case ${TERM} in
  xterm*|rxvt*|Eterm|aterm|kterm|gnome*)
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'

    ;;
  screen*)
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033_%s@%s:%s\033\\" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'
    ;;
esac

[ -r /usr/share/bash-completion/bash_completion   ] && . /usr/share/bash-completion/bash_completion

if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
        source /etc/profile.d/vte.sh
fi
alias ls='ls --color=auto'
alias cp='cp --reflink=auto'
alias grep='grep --color=auto'
alias pacman='pacman --color=auto'

# Using pssh to auto login, with jumper.
#[ -r /usr/share/bash-completion/completions/ssh ] && . /usr/share/bash-completion/completions/ssh
#alias pssh='/opt/bin/pph'
#alias pssh-passwd='/opt/bin/pph-passwd'
#shopt -u hostcomplete && complete -F _ssh pssh pph pssh-passwd pph-passwd

export EDITOR=vim

