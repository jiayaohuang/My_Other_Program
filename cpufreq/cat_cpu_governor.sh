echo -n -e "cpu0:\t"
cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
echo -n -e "\t"
cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq

echo -n -e "cpu1:\t"
cat /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
echo -n -e "\t"
cat /sys/devices/system/cpu/cpu1/cpufreq/cpuinfo_cur_freq

echo -n -e "cpu2:\t"
cat /sys/devices/system/cpu/cpu2/cpufreq/scaling_governor
echo -n -e "\t"
cat /sys/devices/system/cpu/cpu2/cpufreq/cpuinfo_cur_freq

echo -n -e "cpu3:\t"
cat /sys/devices/system/cpu/cpu3/cpufreq/scaling_governor
echo -n -e "\t"
cat /sys/devices/system/cpu/cpu3/cpufreq/cpuinfo_cur_freq
