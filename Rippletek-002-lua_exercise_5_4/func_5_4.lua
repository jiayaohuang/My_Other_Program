#!/usr/bin/env lua


--target_tab = {};

function print_table (p_tab)
	str = "{";
	for i = 1, #(p_tab) do
		str = str .. p_tab[i];
		if p_tab[i+1] == nil then do
			break;
			end;
		else do
			str = str .. ", "
			end;
		end;
	end;
	str = str .. "}"
	print(str);
end;

function print_tables (p_tabs)
	for i = 1, #(p_tabs) do
		print_table(p_tabs[i]);
	end;
end;

local function step (lt, rt, sub_m, target_tab)
	target_tab = target_tab or {};
	for key, value in ipairs(rt) do
		left_tab = {};
		right_tab = {};
		for i = 1, #(lt) do
			left_tab[i] = lt[i];
		end;
		j = 1;
		for i = key, #(rt) do
			right_tab[j] = rt[i];
			j = j + 1;
		end;
		table.insert(left_tab, value);
		table.remove(right_tab, 1);
		if #(left_tab) + #(right_tab) < sub_m then
			do end;
		elseif #(left_tab) == sub_m then
			table.insert(target_tab, left_tab);
		elseif #(left_tab) < sub_m and #(right_tab) ~= 0 then
			step(left_tab, right_tab, sub_m, target_tab);
		end;
	end; --end for
	return;
end;

function combinations (arg_tab, m)
	if type(arg_tab) ~= "table" then
		do
		error("Argument \'tab\' is not table type.");
		return 1;
		end;
	end;
	target_t = {};
	step({}, arg_tab, m, target_t);
	return target_t;
end;

return {print_table = print_table, print_tables = print_tables, combinations = combinations};
