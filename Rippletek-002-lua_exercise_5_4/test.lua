#!/usr/bin/env lua

local func_5_4 = require("func_5_4");

function factorial (n)
	if n == 0 then
		return 1;
	elseif n >= 1 then
		return n * factorial(n-1);
	end;
end;

function count_comb (m, r)
	if m < r then
		error("Expect m >= r, but got m < r.");
		return -1;
	end;
	return (factorial(m) / (factorial(r) * factorial(m-r)));
end;

print("--Test print_table()--");
func_5_4.print_table({});
func_5_4.print_table({1});
func_5_4.print_table({1, 2, 3});
func_5_4.print_table({3, 2, 1});
func_5_4.print_table({'a', 'b', 'c'});
func_5_4.print_table({'abc', 'd', 123});

print("--Test combinations()--");
for k = 1, 4 do
	target_tab = {};
	test_tab = {1, 2, 3, 4};
	target_tab = func_5_4.combinations(test_tab, k);
	assert(#(target_tab) == count_comb(#(test_tab), k));
	for key, line_tab in ipairs(target_tab) do
		assert(#(line_tab) == k);
	end;
end;
print("combinations() tested.");
