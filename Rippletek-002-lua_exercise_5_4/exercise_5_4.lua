#!/usr/bin/env lua

local func_5_4 = require("func_5_4");

target_tab = func_5_4.combinations({1, 2, 3, 4, 5}, 3);
print_tables(target_tab);
