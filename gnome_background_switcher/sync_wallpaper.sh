#!/bin/bash

SRC_DIR=/media/DATA/My_Media/我的图片/lovewallpaper/
DST_DIR=~/Pictures/AutoWallpaper/

if [[ ! -d ${DST_DIR} ]]; then
	mkdir -p ${DST_DIR}
fi

if [[ -d ${SRC_DIR} ]]; then
	rsync -v --times --exclude=".sync" --delete-after -r ${SRC_DIR} ${DST_DIR}
fi

