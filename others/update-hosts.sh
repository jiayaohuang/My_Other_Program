#!/bin/sh
#
# script_tool_for_linux
#
# Using command: `sudo sh script_tool_for_linux.sh` or
#                `su -c 'sh script_tool_for_linux.sh'`
# to update your hosts file.
#
# @WARNING: the script CAN NOT replace other hosts rules.
#           If you have others provided hosts rules, maybe get conflict.
#

if [[ "${USER}x" != "rootx" ]]; then
	sudo $0 $@
	exit $?
fi



if [[ -r /etc/hosts.local ]]; then
	cp /etc/hosts.local /etc/hosts
elif [[ -r /etc/hosts ]]; then
	cp /etc/hosts /etc/hosts.local
else
	echo "127.0.0.1 $(hostname)" > /etc/hosts
	echo "" >> /etc/hosts
fi

echo "# Start hosts from github/racaljk/hosts " >> /etc/hosts
echo "# Last update time: $(date)" >> /etc/hosts

echo "Dowdloading hosts from https://github.com/racaljk/hosts"
echo "Thanks for their works."
wget 'https://raw.githubusercontent.com/racaljk/hosts/master/hosts' \
	-O /tmp/fetchedhosts 2> /dev/null

echo "Download done, start patch hosts."
sed -i '1,/# Modified hosts start/d' /tmp/fetchedhosts
sed -i '/# Modified hosts end/,$d' /tmp/fetchedhosts
cat /tmp/fetchedhosts >> /etc/hosts
echo "Patch hosts done."

rm -f /tmp/fetchedhosts

