#!/bin/bash
export GOPATH=$(cd $(dirname $0); pwd)
echo "GOPATH = ${GOPATH}"
export GOBIN=${GOPATH}/bin
echo "GOBIN = ${GOBIN}"

export GO111MODULE=on

#go env -w GOPROXY=https://goproxy.cn,direct
export http_proxy=socks5://proxy.lan:1081
export https_proxy=socks5://proxy.lan:1081

#go install -v golang.org/x/tools/gopls@v0.7.0
go install -v golang.org/x/tools/gopls@latest
go install -v golang.org/x/tools/cmd/goimports@latest
go install -v golang.org/x/lint/golint@latest
go install -v github.com/ramya-rao-a/go-outline@latest
go install -v github.com/uudashr/gopkgs/v2/cmd/gopkgs@latest
go install -v github.com/sqs/goreturns@latest
go install -v github.com/stamblerre/gocode@latest
go install -v github.com/go-delve/delve/cmd/dlv@latest
go install -v github.com/rogpeppe/godef@latest
go install -v honnef.co/go/tools/cmd/staticcheck@latest

