#!/bin/bash
BLK='\E[1;30m'
RED='\E[1;31m'
GRN='\E[1;32m'
YEL='\E[1;33m'
BLU='\E[1;34m'
MAG='\E[1;35m'
CYN='\E[1;36m'
WHI='\E[1;37m'
DRED='\E[0;31m'
DGRN='\E[0;32m'
DYEL='\E[0;33m'
DBLU='\E[0;34m'
DMAG='\E[0;35m'
DCYN='\E[0;36m'
DWHI='\E[0;37m'
RES='\E[0m'

echo -e " 01- ${BLK} BLK ${RES} "
echo -e " 02- ${RED} RED ${RES} "
echo -e " 03- ${GRN} GRN ${RES} "
echo -e " 04- ${YEL} YEL ${RES} "
echo -e " 05- ${BLU} BLU ${RES} "
echo -e " 06- ${MAG} MAG ${RES} "
echo -e " 07- ${CYN} CYN ${RES} "
echo -e " 08- ${WHI} WHI ${RES} "
echo -e " 09- ${DRED} DRED ${RES} "
echo -e " 10- ${DGRN} DGRN ${RES} "
echo -e " 11- ${DYEL} DYEL ${RES} "
echo -e " 12- ${DBLU} DBLU ${RES} "
echo -e " 13- ${DMAG} DMAG ${RES} "
echo -e " 14- ${DCYN} DCYN ${RES} "
echo -e " 15- ${DWHI} DWHI ${RES} "
