#!/bin/bash

if [[ "${USER}x" != "rootx" ]]; then
	sudo $0 $@
	exit $?
fi

PASSWD=`curl http://www.shadowsocks.asia/ssmm5.php 2> /dev/null | grep '-'`

cat > /etc/shadowsocks/mgm5.sspw.pw.json << EOF
{
    "server":"mgm5.sspw.pw",
    "server_port":443,
    "local_address": "0.0.0.0",
    "local_port":1081,
    "password":"${PASSWD}",
    "timeout":300,
    "method":"aes-256-cfb",
    "fast_open": false,
    "workers": 1
}
EOF

systemctl restart shadowsocks@mgm5.sspw.pw
sleep 1
systemctl status -l --no-pager shadowsocks@mgm5.sspw.pw | grep "Active"
cat /etc/shadowsocks/mgm5.sspw.pw.json

