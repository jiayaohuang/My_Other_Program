#!/bin/bash

SUBFIX=txt
if [[ -n ${1} ]]; then
	SUBFIX=${1}
fi

find -type f | grep -i -e ".${SUBFIX}$" | while read FILENAME
do
	echo -n -e "\0" >> "${FILENAME}"
	echo "Append zero too file ${FILENAME}"
done
