#!/bin/sh

mac=$1
if [ ${#mac} -ne 17 ]; then
	echo "Error format!!!"
	echo "Please use MAC addr format like:"
	echo -e "\tAA:BB:CC:DD:EE:FF"
	echo "MAC addr is the only parameter."
	exit
fi

echo $mac | tr '[a-z]' '[A-Z]' > /tmp/mac-input.txt
sed -i 's/^/\\x/g' /tmp/mac-input.txt
sed -i 's/\:/\\x/g' /tmp/mac-input.txt
machex1=`cat /tmp/mac-input.txt`

pos=`expr ${#machex1} - 1`
tch=${machex1:$pos}
if [[ "$tch" == "9" ]]; then
	tch='A'
elif [[ "$tch" == "F" ]]; then
	tch='0'
else
	tch=`echo $tch | tr '0-8,A-E' '1-9,B-F' `
fi
machex2=`echo $machex1 | sed 's/[0-9,A-F]$/'"$tch"'/'`

pos=`expr ${#machex2} - 1`
tch=${machex2:$pos}
if [[ "$tch" == "9" ]]; then
	tch='A'
elif [[ "$tch" == "F" ]]; then
	tch='0'
else
	tch=`echo $tch | tr '0-8,A-E' '1-9,B-F' `
fi
machex3=`echo $machex2 | sed 's/[0-9,A-F]$/'"$tch"'/'`

echo -n -e $machex1 > /tmp/mac1.img
echo -n -e $machex2 > /tmp/mac2.img
echo -n -e $machex3 > /tmp/mac3.img

dd if=/dev/mtd2 of=/tmp/mtd2.img bs=1 count=4 2> /dev/null
dd if=/tmp/mac3.img of=/tmp/mtd2.img bs=1 count=6 seek=4 2> /dev/null
dd if=/dev/mtd2 of=/tmp/mtd2.img bs=1 skip=10 seek=10 count=30 2> /dev/null
dd if=/tmp/mac1.img of=/tmp/mtd2.img bs=1 count=6 seek=40 2> /dev/null
dd if=/tmp/mac2.img of=/tmp/mtd2.img bs=1 count=6 seek=46 2> /dev/null
dd if=/dev/mtd2 of=/tmp/mtd2.img bs=52 skip=1 seek=1 2> /dev/null
mtd write /tmp/mtd2.img Factory

rm /tmp/mac-input.txt
rm /tmp/mac1.img
rm /tmp/mac2.img
rm /tmp/mac3.img
rm /tmp/mtd2.img

echo "clear up..."
