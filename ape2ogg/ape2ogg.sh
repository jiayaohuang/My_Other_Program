#!/bin/sh

OGG_QUALITY=10
CUE_CHARSET="UTF-8"

PARAM=$#
if [ $PARAM -eq 2 ]; then
   echo ""
   echo ""
   echo "***********************************"
   echo *****Step1 转换ape为wav*****
   echo "***********************************"
   echo ""
   echo ""
   mac $1 `echo $1 | sed -e 's/.ape$//'`.wav -d
   echo ""
   echo ""
   echo "***********************************"
   echo *****Step2 转换wav为ogg*****
   echo "***********************************"
   echo ""
   echo ""
   oggenc -q ${OGG_QUALITY} `echo $1 | sed -e 's/.ape$//'`.wav
   rm `echo $1 | sed -e 's/.ape$//'`.wav
   echo "**************************************************"
   echo *****Step3 将cue文件转换成UTF-8编码*****
   echo "**************************************************"
   echo ""
   echo ""
#   iconv -f GBK -t UTF-8 -o `echo $2 | sed -e 's/.cue$//'`_utf8.cue $2
   iconv -f ${CUE_CHARSET} -t UTF-8 -o `echo $2 | sed -e 's/.cue$//'`_utf8.cue $2
   echo "************************************"
   echo *****Step4 切分ogg文件*****
   echo "************************************"
   mp3splt `echo $1 | sed -e 's/.ape$//'`.ogg -c `echo $2 | sed -e 's/.cue$//'`_utf8.cue -o +'@n - @t - @a'
   rm `echo $2 | sed -e 's/.cue$//'`_utf8.cue
   rm `echo $1 | sed -e 's/.ape$//'`.ogg
   echo ""
   echo ******转换完成！*****

else
   echo -e "usage: ape2ogg filename(ape) filename(cue)"
fi 
